# Einführung in die digitale Textanalyse

## What is this?

- Ergebnisse eines Seminarprojektes ("Einführung in die digitale Textanalyse: Briefe computergestützt lesen", Dr. Maria Hinzmann, https://studiloewe.uni-wuppertal.de:443/qisserver/pages/startFlow.xhtml?_flowId=detailView-flow&unitId=76037&periodId=191&navigationPosition=hisinoneMeinStudium,individualTimetableSchedule)

- Sentimentanalyse von Briefen von und an die Front im zweiten Weltkrieg

## Nutzung
`auswertung/auswertung.py` erfordert Dateien im Ordner `crawlerei/crawls`

- Die Analyseergebnisse der Einzeldateien werden in `csvfile.csv` gespeichert. Die zusammengerechneten Ergebnisse auf Monatsbasis werden in `sortedByMonths.csv` gespeichert.

### Bug

Es besteht ein Bug, in dem nur 11 Monate pro Jahr zusammengerechnet werden. Um zu dem tatsächlichen Ergebnis zu kommen, kam folgender Workflow zum Einsatz:

-  Einzelergebnisse (`csvfile.csv`) in Excel/Calc/Sheets importieren und nach Jahr, dann Monat sortieren. Dazu muss eventuell das Zahlenformat geändert werden, damit 09 vor 10 kommt und nicht 10 vor 9.
-  Hier die Dateinamen löschen (so dass die linkeste Spalte die Jahre sind) und die Daten exportieren
- Wahlweise die Daten als csv speichern und einlesen, ich habe sie einfach direkt in `auswertungFix.py` kopiert und mit RegEx zu Arrays der Form `[Jahr, Monat, Positiv, Neutral, Negativ]` geändert. Im Zweifelsfall das Zahlenformat zurückändern, da 09 sonst nicht als 9 im Dezimalsystem verstanden wird.
- `auswertungFix.py` gibt die summierte csv-like Ausgabe auf die Standard-Ausgabe, hiermit lässt sich dann weiterarbeiten, wie mit der beabsichtigtem Ergebnis von `csvfile.csv`
- Da der Bug erst am Ende des Projektes aufgefallen ist und mit der Sicherheitskopie der Einzelergebnisse umfahren werden konnte, habe ich ihn nicht gefixed. Der Fehler wird bei der Initialisierung der Arrays liegen, welche in Python sehr unintuitiv ist, ist mir aber nicht direkt ersichtlich.

## Ergebnisse

Siehe https://docs.google.com/spreadsheets/d/1xShfxOl-8c1eEIHgdsOQw56zS-HD4R2xbgPiqlavTcE/edit?usp=sharing

## License

CC-BY-NC-SA http://creativecommons.org/licenses/by-nc-sa/4.0/

Simon Krahé