# version 1.0
# Simon Krahé, 17.01.2023
# CC-BY-NC-SA http://creativecommons.org/licenses/by-nc-sa/4.0/

from germansentiment import SentimentModel
import re
import os

model = SentimentModel()

fails = ["So far, none"]
count = 0
csvfile = open("csvfile.csv", "a")
ergebnisse = [[[]]]
for i in range(15):
    temp = []
    for j in range(12):
        temp.append([None])
    ergebnisse.append(temp)
#ergebnisse = [[[None]] * 12] * 15
#ergebnisse = ["jahr", "monat", "tag", "positiv", "neutral", "negativ"]

directory="../crawlerei/crawls"
toten = 0 
for filename in os.listdir(directory):
    if toten<2000: #debug limit
        f = os.path.join(directory, filename)
        # checking if it is a file
        if os.path.isfile(f):
            file = open(f, "r")
            #print(f)
            fulltext = file.read()
            cuttext = re.search('(<div class="brief-titel.*</div>.*?</div>)', fulltext, flags=re.DOTALL).group(1)
            newlinefix = re.sub(r'(<br />)|(\n)', " ", cuttext)
            elementfix = re.sub(r'</?(b|i)>', "", newlinefix)
            briefinhalt = re.search('</h2>(.*?)</div>', elementfix, flags=re.DOTALL).group(1)
            textarray = briefinhalt.split(". ")
            texts = []
            for line in textarray:
                if line != "":
                    texts.append(line + ".")

            try:
                datum = re.search('(\d\d.\d\d.\d\d\d\d)', elementfix, flags=re.DOTALL).group(1)
                datumselemente = re.findall('(\d+)', datum, flags=re.DOTALL)
                tag = datumselemente[0]
                monat = datumselemente[1]
                jahr = datumselemente[2]

                prediction = model.predict_sentiment(texts)

                negative = 0
                neutral = 0
                positive = 0

                for i in range(len(texts)):
                    if(prediction[i] == "neutral"):
                        neutral = neutral + 1
                    if(prediction[i] == "positive"):
                        positive = positive + 1
                    if(prediction[i] == "negative"):
                        negative = negative + 1
                    """
                    print(texts[i] + "\nPrediction: " + prediction[i] + "\n\n")

                gesamt = negative + neutral + positive
                print("Positive: " + str(positive/gesamt*100) + "%\nNeutral: " + str(neutral/gesamt*100) + "%\nNegative: " + str(negative/gesamt*100) + "%")
                """
                jahrpos = int(jahr) - 1938
                monpos = int(monat) - 1
                einzelergebnis = [int(jahr), int(monat), int(tag), int(positive), int(neutral), int(negative)]
                print(ergebnisse[jahrpos][monpos])
                ergebnisse[jahrpos][monpos].append(einzelergebnis)
                csvfile.write(filename + "," + str(jahr) + "," + str(monat) + "," + str(tag) + "," + str(positive) + "," + str(neutral) + "," + str(negative) + "\n")
                file.close
            except AttributeError:
                print(filename + " produced an AttributeError. Check for date formatting")
                fails.append(filename)
            except IndexError:
                print(filename + " out of bounds. Check for date issues.")
                fails.append(filename)
        print("Finished number " + str(count) + ". Progress: " + str(round(count/1700*100, 2)) + "%")
        count = count + 1
        toten = toten + 1
print("Initial analysis done")
print("fails: " + str(fails) + "\ntotal number of fails: " + str(len(fails) + 1))

csvfile = open("sortedByMonths.csv", "a")
csvfile.write("Jahr-Monat,Positiv,Neutral,Negativ\n")
for i in range(len(ergebnisse)-1):
    jahr = ergebnisse[i+1]
    for j in range(len(jahr)-1):
        monat = jahr[j+1]
        if not monat == [None]:
            actualJahr = monat[1][0]
            actualMonat = monat[1][1]
            positive = 0
            negative = 0
            neutral = 0
            for w in range(len(monat)-1):
                brief = monat[w+1]
                positive = positive + brief[3]
                neutral = neutral + brief[4]
                negative = negative + brief[5]
            csvfile.write(str(actualJahr) + "-" + str(actualMonat) + "," + str(positive) + "," + str(neutral) + "," + str(negative) + "\n")