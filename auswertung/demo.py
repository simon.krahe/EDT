from germansentiment import SentimentModel

model = SentimentModel()

texte = [
    "Mir geht es heute sehr gut!",
    "Dieses Projekt ist jetzt schon langweilig.",
    "Diese Scheune ist blau.",
    "Na ganz toll."
    ]
classes, probabilites  = model.predict_sentiment(texte, output_probabilities=True)
print(classes, probabilites)

i = 0
for text in texte:
    print( text + "\n" + classes[i] + ". Einzelscores:\n" + str(probabilites[i][0][0]) + ": " + str(probabilites[i][0][1]) + "\n"+ str(probabilites[i][1][0]) + ": " + str(probabilites[i][1][1]) + "\n"+ str(probabilites[i][2][0]) + ": " + str(probabilites[i][2][1]) + "\n")
    i = i + 1

